package com.example.demo.cicd.thanksforallthefish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThanksForAllTheFishApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThanksForAllTheFishApplication.class, args);
	}

}
