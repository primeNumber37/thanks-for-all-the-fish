package com.example.demo.cicd.thanksforallthefish.controllers;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class SimpleController {

    @GetMapping("/ping")
    public Map<String, String> ping() {
        Map<String, String> body = new HashMap<>();

        body.put("appName", "thanks");
        body.put("version", "0.0.1-SNAPSHOT");

        return body;
    }
}
